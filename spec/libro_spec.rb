require 'spec_helper'
require 'libro'

describe Libro do

describe "# Almacenamiento de Autores" do
    it "Se almacena correctamente el autor o autores" do
        L1 = Libro::Libro.new()
 expect(L1.a[0]).to eq("Dave Thomas")
  expect(L1.a[1]).to eq("Andy Hunt")
   expect(L1.a[2]).to eq( "Chad Fowler")
    end
  end


describe "# Almacenamiento de Título" do
    it "Se almacena correctamente el Título" do
 expect(L1.t).to eq("Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide")
    end
  end

describe "# Almacenamiento de Serie" do
    it "Se almacena correctamente el Título" do
 expect(L1.s).to eq("(The Facets of Ruby)")
    end
  end
  
  describe "# Almacenamiento de Editorial" do
    it "Se almacena correctamente la Editorial" do
 expect(L1.e).to eq("Pragmatic Bookshelf")
    end
  end

 describe "# Almacenamiento de Edicion" do
    it "Se almacena correctamente la Edicion" do
 expect(L1.ed).to eq("4 edition")
    end
  end

describe "# Almacenamiento de Fecha de Publicación" do
    it "Se almacena correctamente la Fecha de Publicación" do
 expect(L1.f).to eq("(July 7, 2013)")
    end
  end
  
  describe "# Almacenamiento de ISBN" do
    it "Se almacena correctamente el ISBN" do
 expect(L1.vect[0]).to eq("ISBN-13:978-1937785499")
 expect(L1.vect[1]).to eq("ISBN-10:1937785491")
    end
  end
#####################################################################################  
    describe "# Obtener listado de autores" do
        it "Se obtiene un listado con los autores del libro" do
     expect(L1.geta).to eq("[\"Dave Thomas\", \"Andy Hunt\", \"Chad Fowler\"]")
        end
      end  
      
    describe "# Metodo obtener titulo" do
        it "Se obtiene titulo del libro" do
     expect(L1.gett).to eq("Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide")
        end
      end  
      
    describe "# Metodo obtener serie" do
        it "Se obtiene la serie del libro" do
     expect(L1.getss).to eq("(The Facets of Ruby)")
        end
      end 
      
     describe "# Metodo obtener editorial" do
        it "Se obtiene la editorial del libro" do
     expect(L1.geteditorial).to eq("Pragmatic Bookshelf")
        end
      end 
      
     describe "# Metodo obtener edicion" do
        it "Se obtiene el numero de edicion del libro" do
     expect(L1.getedicion).to eq("4 edition")
        end
      end 
      
     describe "# Metodo obtener fecha de publicacion" do
        it "Se obtiene la fecha de publicacion del libro" do
     expect(L1.getfecha).to eq("(July 7, 2013)")
        end
      end
      
     describe "# Metodo obtener ISBN" do
        it "Se obtiene el ISBN del libro" do
     expect(L1.getISBN).to eq("[\"ISBN-13:978-1937785499\", \"ISBN-10:1937785491\"]")
        end
      end 
######################################################################################
  describe "# Formateo salida" do
    it "Se formatea la salida por pantalla de la información del libro" do
        expect(L1.formatea).to eq("Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide, [\"Dave Thomas\", \"Andy Hunt\", \"Chad Fowler\"]\n(The Facets of Ruby)\nPragmatic Bookshelf; 4 edition (July 7, 2013)\n[\"ISBN-13:978-1937785499\", \"ISBN-10:1937785491\"]")
    end
  end

end
