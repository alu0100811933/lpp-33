module Libro

 
  class Libro
   
    attr_reader :a,:t, :s, :e, :ed, :f, :vect
    
   
    def initialize()
     
      @a = [ 'Dave Thomas', 'Andy Hunt', 'Chad Fowler']
      @t = "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide"
      @s = "(The Facets of Ruby)"
      @e = "Pragmatic Bookshelf"
      @ed = "4 edition"
      @f = "(July 7, 2013)"
      @vect = %w{ ISBN-13:978-1937785499 ISBN-10:1937785491 }
    end
    
    def geta
      return "#{@a}"
    end

    def gett
      return "#{@t}"
    end

    def getss
      return "#{@s}"
    end
    
    def geteditorial
      return "#{@e}"
    end
    
    def getedicion
      return "#{@ed}"
    end
    
    def getfecha
      return "#{@f}"
    end
    
    def getISBN
      return "#{@vect}"
    end
    
    def formatea()
        puts "#{@t}, #{@a}\n#{@s}\n#{@e}; #{@ed} #{@f}\n#{@vect}"
        return "#{@t}, #{@a}\n#{@s}\n#{@e}; #{@ed} #{@f}\n#{@vect}"
    end

  end
end
